import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

public class DepDescComp implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {
        String splitString = "/";

        String[] array1 = o1.split(splitString);
        String[] array2 = o2.split(splitString);

        int cmp = array2[0].compareTo(array1[0]);
        if (cmp == 0) {
            cmp = getEndString(array1, 1).compareTo(getEndString(array2, 1));
        }

        return cmp;
    }

    private String getEndString(String[] array, int skipElements) {
        return Arrays.asList(array).stream()
                .skip(skipElements)
                .collect(Collectors.joining("/"));
    }
}
