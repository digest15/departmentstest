import java.util.*;

public class Departments {
    public static List<String> fillGaps(List<String> deps) {
        Objects.requireNonNull(deps);

        Set<String> tmp = new LinkedHashSet<>();
        for (String value : deps) {
            StringJoiner stringJoiner = new StringJoiner("/");
            for (String el : value.split("/")) {
                el = el.trim();
                stringJoiner.add(el);
                tmp.add(stringJoiner.toString());
            }
        }
        return new ArrayList<>(tmp);
    }

    public static void sortAsc(List<String> orgs) {
        Objects.requireNonNull(orgs);

        orgs.sort(String::compareTo);
    }

    public static void sortDesc(List<String> orgs) {
        Objects.requireNonNull(orgs);

        orgs.sort(new DepDescComp());
    }
}
